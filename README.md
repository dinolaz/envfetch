# zydfetch
Utility written in Lua for FreeBSD which show system info.

## Dependencies
- lua54
- wmctrl
- git
## Use
```
git clone https://gitlab.com/dinolaz/zydfetch/

./zydfetch/zydfetch.lua
```

## Screenshot
![IMAGE_DESCRIPTION](https://gitlab.com/dinolaz/zydfetch/-/raw/main/zydfetch.png)
