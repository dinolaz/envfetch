#!/usr/local/bin/lua54
--libraries
local lunacolors = require 'Lunacolors'
lunabold = lunacolors.bold

--envs
HOST = io.popen("hostname"):read("*a"):sub(1, -2)
SHELL = os.getenv("SHELL")
TERMINAL = os.getenv("TERMINAL")
USER = os.getenv("USER")
EDITOR = os.getenv("EDITOR")
OS = io.popen("uname -sr"):read("*a"):sub(1, -2)
LANG = os.getenv("LANG")
WM = io.popen("wmctrl -m | grep Name | cut -d: -f2"):read("*a"):sub(1, -2)
PACKAGES = io.popen("pkg info | wc -l | sed -e 's/^[ \t]*//'"):read("*a"):sub(1, -2)

--colors
magenta = lunacolors.magenta
red = lunacolors.red
yellow = lunacolors.yellow
green = lunacolors.green
blue = lunacolors.blue

--[[
print (lunabold(green ( USER .. "@" .. HOST)))
print (lunabold(magenta( "~~~~~~~~~~~~~~~~~")))
print (lunabold(magenta("OS ") .. yellow("    ~>  ") .. OS))
print (lunabold(magenta("WM ") .. yellow("    ~> ") .. WM))
print (lunabold(magenta("Lang ") .. yellow("  ~>  ") .. LANG))
print (lunabold(magenta("Shell ") .. yellow(" ~>  ") .. SHELL))
print (lunabold(magenta("Term ") .. yellow("  ~>  ") .. TERMINAL))
print (lunabold(magenta("Editor ") .. yellow("~>  ") ..EDITOR))
print (lunabold(magenta("Packages ") .. yellow("   ~> ") .. PACKAGES))
print (lunabold(magenta("Lua Version ") .. yellow("~> ") .._VERSION))
]]

--output
os.execute("clear")
print((lunabold(magenta("⣇⣿⠘⣿⣿⣿⡿⡿⣟⣟⢟⢟⢝⠵⡝⣿⡿⢂⣼⣿⣷⣌⠩⡫⡻⣝⠹⢿⣿⣷") .. lunabold(magenta ("   " .. USER .. "@" .. HOST)))))
print(((lunabold(magenta("⡆⣿⣆⠱⣝⡵⣝⢅⠙⣿⢕⢕⢕⢕⢝⣥⢒⠅⣿⣿⣿⡿⣳⣌⠪⡪⣡⢑⢝⣇")))))
print(((lunabold(magenta("⡆⣿⣿⣦⠹⣳⣳⣕⢅⠈⢗⢕⢕⢕⢕⢕⢈⢆⠟⠋⠉⠁⠉⠉⠁⠈⠼⢐⢕⢽") .. magenta("   OS ") .. yellow("     ~> ") .. OS))))
print(((lunabold(magenta("⡗⢰⣶⣶⣦⣝⢝⢕⢕⠅⡆⢕⢕⢕⢕⢕⣴⠏⣠⡶⠛⡉⡉⡛⢶⣦⡀⠐⣕⢕") .. magenta("   WM ") .. yellow("     ~>") .. WM))))
print(((lunabold(magenta("⡝⡄⢻⢟⣿⣿⣷⣕⣕⣅⣿⣔⣕⣵⣵⣿⣿⢠⣿⢠⣮⡈⣌⠨⠅⠹⣷⡀⢱⢕") .. magenta("   Shell") .. yellow("   ~> ") .. SHELL))))
print(((lunabold(magenta("⡝⡵⠟⠈⢀⣀⣀⡀⠉⢿⣿⣿⣿⣿⣿⣿⣿⣼⣿⢈⡋⠴⢿⡟⣡⡇⣿⡇⡀⢕") .. magenta("   Lang") .. yellow("    ~> ") .. LANG))))
print(((lunabold(magenta("⡝⠁⣠⣾⠟⡉⡉⡉⠻⣦⣻⣿⣿⣿⣿⣿⣿⣿⣿⣧⠸⣿⣦⣥⣿⡇⡿⣰⢗⢄") .. magenta("   Term") .. yellow("    ~> ") .. TERMINAL))))
print(((lunabold(magenta("⠁⢰⣿⡏⣴⣌⠈⣌⠡⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣬⣉⣉⣁⣄⢖⢕⢕⢕") .. magenta("   Editor") .. yellow("  ~> ") .. EDITOR))))
print(((lunabold(magenta("⡀⢻⣿⡇⢙⠁⠴⢿⡟⣡⡆⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣵⣵⣿") .. magenta("   Packages") .. yellow("    ~> ") .. PACKAGES))))
print(((lunabold(magenta("⡻⣄⣻⣿⣌⠘⢿⣷⣥⣿⠇⣿⣿⣿⣿⣿⣿⠛⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿") .. magenta("   Lua Version") .. yellow(" ~> ") .. _VERSION))))
print(((lunabold(magenta("⣷⢄⠻⣿⣟⠿⠦⠍⠉⣡⣾⣿⣿⣿⣿⣿⣿⢸⣿⣦⠙⣿⣿⣿⣿⣿⣿⣿⣿⠟") ))))
print(((lunabold(magenta("⡕⡑⣑⣈⣻⢗⢟⢞⢝⣻⣿⣿⣿⣿⣿⣿⣿⠸⣿⠿⠃⣿⣿⣿⣿⣿⣿⡿⠁⣠") ))))
print(((lunabold(magenta("⡝⡵⡈⢟⢕⢕⢕⢕⣵⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣿⣿⣿⣿⣿⠿⠋⣀⣈⠙") ))))
print(((lunabold(magenta("⡝⡵⡕⡀⠑⠳⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⢉⡠⡲⡫⡪⡪⡣") ))))
